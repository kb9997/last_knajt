# Last_knajt

Simple game created with SpriteKit.

## How to play

Tap on left side of screen to rotate knight.
Tap on right side to attack.

## Features

Two game modes:
- normal (three lives),
- hardcore (one life and faster enemies),
- local leaderboard.

## Info 

Created for IPhones only. Tested on IPhone SE with iOS 12.0.

## Known issue

- on Iphones with notch (X and later) game is scalling to superView.



