//
//  OverScene.swift
//  
//
//  Created by kb9997 on 03.03.2019.
//

import Foundation
import SpriteKit
import GameplayKit

class OverScene: SKScene, UITextFieldDelegate {
    
    var yourScoreLabel:SKLabelNode!
    var scoreLabel:SKLabelNode!
    var backToMenuLabel:SKLabelNode!
    var saveScore:SKLabelNode!
    var background:SKSpriteNode!
    var userNameTextField:UITextField!
    var score = 0
    
    override func didMove(to view: SKView) {
        
        self.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        background = SKSpriteNode(imageNamed: "menuBackground")
        background.position = CGPoint(x: 0, y: 0)
        background.zPosition = -1
        background.size =  CGSize(width: self.frame.size.width, height: self.frame.size.height)
        
        yourScoreLabel = SKLabelNode(text: "Your Score:")
        yourScoreLabel.fontName = "Georgia-Bold"
        yourScoreLabel.fontSize = 30
        yourScoreLabel.fontColor = UIColor.white
        yourScoreLabel.position = CGPoint(x: 0, y: self.frame.height/2-yourScoreLabel.fontSize)
        yourScoreLabel.zPosition = 2

        scoreLabel = SKLabelNode(text: "\(score)")
        scoreLabel.fontName = "Georgia-Bold"
        scoreLabel.fontSize = 50
        scoreLabel.fontColor = UIColor.red
        scoreLabel.position = CGPoint(x: 0, y: self.frame.height/2-(yourScoreLabel.fontSize+scoreLabel.fontSize))
        scoreLabel.zPosition = 2

        backToMenuLabel = SKLabelNode(text: "Back to Menu")
        backToMenuLabel.fontName = "Georgia-Bold"
        backToMenuLabel.fontSize = 30
        backToMenuLabel.fontColor = UIColor.white
        backToMenuLabel.position = CGPoint(x: -self.frame.width/4, y: -self.frame.height/2+backToMenuLabel.fontSize)
        backToMenuLabel.name = "backToMenu"
        
        saveScore = SKLabelNode(text: "Save your score")
        saveScore.fontName = "Georgia-Bold"
        saveScore.fontSize = 30
        saveScore.fontColor = UIColor.white
        saveScore.position = CGPoint(x: (self.frame.width/4), y: -self.frame.height/2+backToMenuLabel.fontSize)
        saveScore.name = "saveScore"
        
        let textFieldFrame = CGRect(x: (self.view?.frame.width ?? 0)/4, y: 0+self.yourScoreLabel.frame.height+(self.scoreLabel.frame.height*2), width: self.frame.size.width/2, height: scoreLabel.frame.height*1.5)
        userNameTextField = UITextField(frame: textFieldFrame)
        
        view.addSubview(userNameTextField)
        userNameTextField.layer.cornerRadius = 10.0
        userNameTextField.layer.borderWidth = 2
        userNameTextField.layer.borderColor = UIColor.black.cgColor
        userNameTextField.delegate = self
        userNameTextField.textAlignment = .center
        userNameTextField.placeholder = "Enter your name  here"
        userNameTextField.backgroundColor = .lightGray
        userNameTextField.textColor = .black
        userNameTextField.clearButtonMode = .whileEditing
        userNameTextField.returnKeyType = .done
        
        self.view?.addSubview(userNameTextField)
        
        self.addChild(background)
        self.addChild(yourScoreLabel)
        self.addChild(scoreLabel)
        self.addChild(backToMenuLabel)
        self.addChild(saveScore)
        
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches{
            let location = touch.location(in: self)
            let touchedNode = self.nodes(at: location)
            let transition1 = SKTransition.flipHorizontal(withDuration: 0.5)
            for node in touchedNode{
                
                if node.name == "backToMenu" {
                    
                    // TRANSITION TO MENU
                    let menuScene = MenuScene()
                    menuScene.scaleMode = .resizeFill
                    self.view?.presentScene(menuScene, transition: transition1)
                    userNameTextField.removeFromSuperview()
                    self.removeAllChildren()
                    
                }else if node.name == "saveScore"{
                    
                    // SAVING SCORE TO USER DEFAULTS
                    let userDefaults = UserDefaults.standard
                    if let leaderBoardData = userDefaults.value(forKey: "leaderBoard") as? Data {
                        var leaderBoardData2 = try? PropertyListDecoder().decode(Array<playerData>.self, from: leaderBoardData)
                        
                        
                        if (userNameTextField.text == ""){
                            leaderBoardData2?.append(playerData(name: "Nameless Knajt", score: score))
                        }else{
                            leaderBoardData2?.append(playerData(name: userNameTextField.text ?? "noNickname", score: score))
                        }
                        
                        leaderBoardData2?.sort(by: {$0.score > $1.score})
                        
                        userDefaults.set(try? PropertyListEncoder().encode(leaderBoardData2), forKey: "leaderBoard")
                    }

                    // TRANSITION TO LEADERBOARD SCENE
                    let leaderboardScene1 = LeaderboardScene()
                    leaderboardScene1.scaleMode = .resizeFill
                    self.view?.presentScene(leaderboardScene1, transition: transition1)
                    userNameTextField.removeFromSuperview()
                    self.removeAllChildren()
                }else{
                    userNameTextField.endEditing(true)
                }
                
            }
        }
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
    
}

