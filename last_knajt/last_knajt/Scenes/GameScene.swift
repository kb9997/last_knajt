//
//  GameScene.swift
//  last_knajt
//
//  Created by kb9997 on 15/02/2019.
//  Copyright © 2019 macKawa. All rights reserved.
//

import SpriteKit
import GameplayKit


protocol GameDelegate{
    func gameOver()
}

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var background: SKSpriteNode!
    var player: SKSpriteNode!
    var sword: SKSpriteNode!

    var gameTimer:Timer!
    var side: Bool = true
    var gameDelegate: GameDelegate?
    
    var score:Int = 0
    var lives:Int = 3
    var scoreLabel:SKLabelNode!
    var hardLabel:SKLabelNode!
    var livesHead:SKSpriteNode!
    var livesHead2:SKSpriteNode!
    var livesHead3:SKSpriteNode!
    var pauseButton:SKSpriteNode!
    var hardMode:Bool = false
    var gamePaused:Bool = false {
        didSet{
            self.isPaused = gamePaused
        }
    }
    
    // PAUSE SCREEN
    
    var pauseBackground:SKSpriteNode!
    var pauseResumeLabel:SKLabelNode!
    var pauseExitLabel:SKLabelNode!
    
    let enemyCategory:UInt32  = 0x00000001
    let playerCategory:UInt32 = 0x00000010
    let swordCategory:UInt32  = 0x00000100
    var enemySpawnInterval:TimeInterval = 1
    
    var swordAnimation: SKAction!
    var sword_textures:[SKTexture]! = []

    override func didMove(to view: SKView) {
        
        background = SKSpriteNode(imageNamed: "background")
        background.position = CGPoint(x: 0, y: 0)
        background.scale(to: CGSize(width: self.frame.size.width, height: self.frame.size.height))
        background.zPosition = 1
        
        player = SKSpriteNode(imageNamed: "knajt1")
        player.position = CGPoint(x: 0, y: -(self.frame.size.width/2)*0.35)
        player.zPosition = 2
        
        player.physicsBody = SKPhysicsBody(texture: SKTexture(imageNamed: "knajt1"), size: CGSize(width: player.size.width, height: player.size.height))
        player.physicsBody?.isDynamic = true
        player.physicsBody?.categoryBitMask = playerCategory
        player.physicsBody?.contactTestBitMask = enemyCategory
        player.physicsBody?.collisionBitMask = 0
        player.physicsBody?.usesPreciseCollisionDetection = true
        
            sword_textures.append(SKTexture(imageNamed: "sword1"))
            sword_textures.append(SKTexture(imageNamed: "sword2"))
            sword_textures.append(SKTexture(imageNamed: "sword3"))
            sword_textures.append(SKTexture(imageNamed: "sword4"))
            sword_textures.append(SKTexture(imageNamed: "sword5"))
            sword_textures.append(SKTexture(imageNamed: "sword6"))
            sword_textures.append(SKTexture(imageNamed: "sword1"))
        
            swordAnimation = SKAction.animate(with: sword_textures, timePerFrame: 0.5)
        
        sword = SKSpriteNode(imageNamed: "sword1")
        sword.position = CGPoint(x: player.position.x+40, y: player.position.y)
        
        sword.zPosition = 2
        
        self.physicsWorld.contactDelegate = self
        self.physicsWorld.gravity = CGVector(dx: 0, dy: 0)
        
        //  INTERFACE INTERFACE INTERFACE INTERFACE INTERFACE
        
        scoreLabel = SKLabelNode(text: NSString(format: "Score: %i", score) as String)
        scoreLabel.position = CGPoint(x: 0, y: (frame.size.height/2)-30)
        scoreLabel.fontName = "Georgia-Bold"
        scoreLabel.fontSize = 30
        scoreLabel.fontColor = UIColor.white
        scoreLabel.zPosition = 7
        
        livesHead = SKSpriteNode(imageNamed: "head")
        livesHead.scale(to: CGSize(width: 40, height: 40))
        livesHead.position = CGPoint(x: -(frame.size.width/2)+(livesHead.size.width/2) , y: (frame.size.height/2)-(livesHead.size.height/2))
        livesHead.zPosition = 5
        
        livesHead2 = SKSpriteNode(imageNamed: "head")
        livesHead2.scale(to: CGSize(width: 40, height: 40))
        livesHead2.position = CGPoint(x: livesHead.position.x+livesHead.frame.width  , y: livesHead.position.y)
        livesHead2.zPosition = 5
        
        livesHead3 = SKSpriteNode(imageNamed: "head")
        livesHead3.scale(to: CGSize(width: 40, height: 40))
        livesHead3.position = CGPoint(x: livesHead.position.x+(2 * livesHead.frame.width)  , y: livesHead.position.y)
        livesHead3.zPosition = 5
        
        pauseButton = SKSpriteNode(imageNamed: "pause")
        pauseButton.scale(to: CGSize(width: 40, height: 40))
        pauseButton.position = CGPoint(x: (frame.size.width/2)-(pauseButton.size.width/2)  , y: livesHead.position.y)
        pauseButton.zPosition = 5
        pauseButton.name = "pauseButton"
        
        // pause screen
        
        pauseBackground = SKSpriteNode(imageNamed: "pauseBackground")
        pauseBackground.color = .black
        pauseBackground.scale(to: CGSize(width: self.frame.size.width, height: self.frame.size.height))
        pauseBackground.zPosition = 6
        
        pauseResumeLabel = SKLabelNode(text: "Resume")
        pauseResumeLabel.position = CGPoint(x: (frame.size.width/4), y: 0)
        pauseResumeLabel.fontName = "Georgia-Bold"
        pauseResumeLabel.fontSize = 40
        pauseResumeLabel.fontColor = UIColor.white
        pauseResumeLabel.zPosition = 7
        pauseResumeLabel.name = "pauseResume"
        
        pauseExitLabel = SKLabelNode(text: "Exit")
        pauseExitLabel.position = CGPoint(x: -(frame.size.width/4), y: 0)
        pauseExitLabel.fontName = "Georgia-Bold"
        pauseExitLabel.fontSize = 40
        pauseExitLabel.fontColor = UIColor.white
        pauseExitLabel.zPosition = 7
        pauseExitLabel.name = "pauseExit"
        
        if hardMode == true {
            hardLabel = SKLabelNode(text: "HARDCORE!")
            hardLabel.position = CGPoint(x: 0, y: (frame.size.height/2)-(1.5*hardLabel.frame.height)-scoreLabel.frame.height)
            hardLabel.fontName = "Georgia-Bold"
            hardLabel.fontSize = 30
            hardLabel.fontColor = UIColor.red
            hardLabel.zPosition = 4
            self.addChild(hardLabel)
            
            enemySpawnInterval = 0.3
            lives = 1
        }
        
        self.addChild(background)
        self.addChild(player)
        self.addChild(sword)
        self.addChild(scoreLabel)
        self.addChild(livesHead)
        if hardMode == false{
            self.addChild(livesHead2)
            self.addChild(livesHead3)
        }
        self.addChild(pauseButton)
        
        // TIMER TIMER TIMER TIMER TIMER TIMER
        gameTimer = Timer.scheduledTimer(timeInterval: enemySpawnInterval, target: self, selector: #selector(addEnemy), userInfo: nil, repeats: true)
    }
    
    @objc func addEnemy(){
        
        let temp_spawn = Int.random(in: 0 ... 1)
        
        if temp_spawn == 1{
            
            let temp_side = Int.random(in: 0 ... 1)
            let temp_type = Int.random(in: 0 ... 1)
            
            let enemy:SKSpriteNode
            var actionArray = [SKAction]()
            
            if temp_type == 1{
                enemy = SKSpriteNode(imageNamed: "enemy1")
                let animationDuration:TimeInterval = 6
                
                if temp_side == 0{ // z lewej
                    enemy.position = CGPoint(x: -self.frame.size.width+enemy.size.width, y: player.position.y-(player.size.height/2))
                    enemy.scale(to: CGSize(width: -enemy.size.width, height: enemy.size.height))
                    actionArray.append(SKAction.move(to: CGPoint(x: player.frame.midX, y: player.frame.minY), duration: animationDuration))
                }else{              // z prawej
                    enemy.position = CGPoint(x: self.frame.size.width+enemy.size.width, y: player.position.y-(player.size.height/2))
                    enemy.scale(to: CGSize(width: enemy.size.width, height: enemy.size.height))
                    actionArray.append(SKAction.move(to: CGPoint(x: player.frame.midX, y: player.frame.minY), duration: animationDuration))
                }
            }else{
                enemy = SKSpriteNode(imageNamed: "bat1")
                let animationDuration:TimeInterval = 4
                
                if temp_side == 0{ // z lewej
                    enemy.position = CGPoint(x: -self.frame.size.width+enemy.size.width, y: -(player.size.height/2))
                    enemy.scale(to: CGSize(width: -(0.5*enemy.size.width), height: (0.5*enemy.size.height)))
                    actionArray.append(SKAction.move(to: CGPoint(x: player.frame.midX, y: player.frame.midY), duration: animationDuration))
                }else{              // z prawej
                    enemy.position = CGPoint(x: self.frame.size.width+enemy.size.width, y: -(player.size.height/2))
                    enemy.scale(to: CGSize(width: 0.5*enemy.size.width, height: 0.5*enemy.size.height))
                    actionArray.append(SKAction.move(to: CGPoint(x: player.frame.midX, y: player.frame.midY), duration: animationDuration))
                }
            }
            
            enemy.anchorPoint = CGPoint(x: 0.5, y: 0)

            enemy.zPosition = 2
            enemy.physicsBody = SKPhysicsBody(rectangleOf: enemy.size)
            enemy.physicsBody?.isDynamic = true
            enemy.physicsBody?.categoryBitMask = enemyCategory
            
            enemy.physicsBody?.contactTestBitMask = swordCategory
            enemy.physicsBody?.collisionBitMask = 0
            self.addChild(enemy)
            
            actionArray.append(SKAction.removeFromParent())
            
            enemy.run(SKAction.sequence(actionArray))
    }else{}
    }
    
    func attack(){

        let new_sword = SKSpriteNode(imageNamed: "sword1")
        new_sword.zPosition = 2
        if side == true{ // hero paczy w prawo
            new_sword.position = sword.position
            new_sword.scale(to: CGSize(width: sword.size.width, height: sword.size.height))

        }else{          // hero paczy w lewo
            new_sword.position = sword.position
            new_sword.scale(to: CGSize(width: -sword.size.width, height: sword.size.height))

        }
        
        new_sword.physicsBody = SKPhysicsBody(rectangleOf: new_sword.size)
        new_sword.physicsBody?.isDynamic = true
        new_sword.physicsBody?.categoryBitMask = swordCategory
        new_sword.physicsBody?.contactTestBitMask = enemyCategory
        new_sword.physicsBody?.collisionBitMask = 0
        new_sword.physicsBody?.usesPreciseCollisionDetection = true
        
        self.addChild(new_sword)
        
        var actionArray = [SKAction]()
        actionArray.append(SKAction.animate(with: sword_textures, timePerFrame: 0.02))
        actionArray.append(SKAction.removeFromParent())
        new_sword.run(SKAction.sequence(actionArray))
        
    }

    func pauseGame(){
        gamePaused = true
        physicsWorld.speed = 0
        
        self.addChild(pauseBackground)
        self.addChild(pauseExitLabel)
        self.addChild(pauseResumeLabel)
    }
    
    func resumeGame(){
        
        gamePaused = false
        physicsWorld.speed = 1
        
        pauseBackground.removeFromParent()
        pauseExitLabel.removeFromParent()
        pauseResumeLabel.removeFromParent()
    }
    
    override var isPaused: Bool {
        didSet{
            if (self.isPaused == false && self.gamePaused == true){
                self.isPaused = true
            }else{
                
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches{
            let location = touch.location(in: self)
            let touchedNode = self.nodes(at: location)
            
            for node in touchedNode{
                
                if gamePaused == false{
                    if node.name == "pauseButton" {
                    pauseGame()
                    return
                    }else{
                        
                        if location.x < 0 { // lewa strona ekranu
                            
                            if side == true{ // obrot w lewo
                                player.scale(to: CGSize(width: -player.size.width, height: player.size.height))
                                sword.position = CGPoint(x: -(player.position.x+40), y: sword.position.y)
                                sword.scale(to: CGSize(width: -sword.size.width, height: sword.size.height))
                                side = false
                                return
                                
                            }else{          // obrot w prawo
                                player.scale(to: CGSize(width: player.size.width, height: player.size.height))
                                sword.position = CGPoint(x: player.position.x+40, y: sword.position.y)
                                sword.scale(to: CGSize(width: sword.size.width, height: sword.size.height))
                                side = true
                                return
                            }
                        }else{      // prawa strona ekranu
                            attack()
                            return
                        }
                        
                    }}else{
                    if node.name == "pauseResume"{
                        resumeGame()
                        return
                    }else if node.name == "pauseExit"{
                        mygameOver()
                        return
                    }else{
                        
                    }
                }
                
            }
            
        }
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        let collision:UInt32 = contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask
        
        var enemyBody:SKPhysicsBody?
        var swordBody:SKPhysicsBody?
        
        if collision == enemyCategory | swordCategory{

            if (contact.bodyA.categoryBitMask == swordCategory) && (contact.bodyB.categoryBitMask == enemyCategory){
                    swordBody = contact.bodyA
                    enemyBody = contact.bodyB

                swordWithEnemyCollision(swordNode: swordBody!.node as? SKSpriteNode, enemyNode: enemyBody!.node as? SKSpriteNode)
                
            }else if (contact.bodyB.categoryBitMask == swordCategory) && (contact.bodyA.categoryBitMask == enemyCategory){
                swordBody = contact.bodyB
                enemyBody = contact.bodyA

                swordWithEnemyCollision(swordNode: swordBody!.node as? SKSpriteNode, enemyNode: enemyBody!.node as? SKSpriteNode)
            }else{}
            

        }else if( collision == enemyCategory | playerCategory){
            
            if (contact.bodyA.categoryBitMask == enemyCategory){
                enemyBody = contact.bodyA
            }else{
                enemyBody = contact.bodyB
            }
            if let enemyNode = enemyBody!.node as? SKSpriteNode{
                playerWithEnemyCollision(enemyNode: enemyNode)
            }
            
        }
    }
    
    func playerWithEnemyCollision(enemyNode: SKSpriteNode){
      
        let resizeAction = SKAction.resize(toHeight: 0, duration: 0.3)
        enemyNode.physicsBody = nil
        enemyNode.removeAllActions()
        enemyNode.run(resizeAction, completion: {
            enemyNode.removeFromParent()
        })
        lives -= 1
        if lives <= 0{
            player.physicsBody = nil
            livesHead.removeFromParent()
            mygameOver()
        }
        if (lives == 2){
         livesHead3.removeFromParent()
        }else if(lives == 1){
         livesHead2.removeFromParent()
        }
    }
    
    func swordWithEnemyCollision(swordNode: SKSpriteNode?, enemyNode: SKSpriteNode?){
        
        let resizeAction = SKAction.resize(toHeight: 0, duration: 0.3)
        guard enemyNode != nil else{
            return
        }
        enemyNode!.physicsBody = nil
        enemyNode!.removeAllActions()
        enemyNode!.run(resizeAction, completion: {
            enemyNode!.removeFromParent()
            })

        score += 1
        scoreLabel.text = NSString(format: "Score: %i", score) as String
    }
    
    func mygameOver(){
        
        let transition2 = SKTransition.flipHorizontal(withDuration: 0.5)
        let overScene1 = OverScene()
        overScene1.score = self.score
        overScene1.scaleMode = .resizeFill
        self.view?.presentScene(overScene1, transition: transition2)
            self.removeAllChildren()
            self.removeAllActions()
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
}
