import Foundation
import SpriteKit
import GameplayKit

class SettingsScene: SKScene {

    var background:SKSpriteNode!
    var titleLabel:SKLabelNode!
    var backToMenuLabel:SKLabelNode!
    var settingsBackground:SKSpriteNode!
    var clearLeaderBoardButton:SKLabelNode!
    var credits:SKLabelNode!
    
    override func didMove(to view: SKView) {
        

        self.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        background = SKSpriteNode(imageNamed: "menuBackground")
        background.position = CGPoint(x: 0, y: 0)
        background.zPosition = -1
        background.size =  CGSize(width: self.frame.size.width, height: self.frame.size.height)
        
        titleLabel = SKLabelNode(text: "Settings")
        titleLabel.fontName = "Georgia-Bold"
        titleLabel.fontSize = 30
        titleLabel.fontColor = UIColor.white
        titleLabel.position = CGPoint(x: 0, y: self.frame.height/2-(1.5*titleLabel.fontSize))
        titleLabel.zPosition = 2
        
        backToMenuLabel = SKLabelNode(text: "Back to Menu")
        backToMenuLabel.fontName = "Georgia-Bold"
        backToMenuLabel.fontSize = 30
        backToMenuLabel.fontColor = UIColor.white
        backToMenuLabel.position = CGPoint(x: (self.frame.width/4)-50, y: -self.frame.height/2+backToMenuLabel.fontSize/2)
        backToMenuLabel.name = "backToMenu"
        
        let settingsBackgroundWidth = self.frame.width*0.6 - 20
        let settingsBackgroundHeight = 2*(self.frame.height/3) - 20
        settingsBackground = SKSpriteNode(color: UIColor.black, size: CGSize(width: settingsBackgroundWidth, height: settingsBackgroundHeight))
        settingsBackground.zPosition = 1
        settingsBackground.position = CGPoint(x: (self.frame.width/4)-50, y: 0)
        
        clearLeaderBoardButton = SKLabelNode(text: "Clear Leaderboard")
        clearLeaderBoardButton.fontName = "Georgia-Bold"
        clearLeaderBoardButton.fontSize = 30
        clearLeaderBoardButton.fontColor = UIColor.white
        clearLeaderBoardButton.position = CGPoint(x: settingsBackground.frame.midX, y: settingsBackground.frame.maxY-(1.5*clearLeaderBoardButton.frame.height))
        clearLeaderBoardButton.zPosition = 2
        clearLeaderBoardButton.name = "clearLeaderBoard"
        
        credits = SKLabelNode(text: "Made by: Dezeriusz/kb9997")
        credits.fontName = "Georgia-Bold"
        credits.fontSize = 15
        credits.fontColor = UIColor.gray
        credits.position = CGPoint(x: settingsBackground.frame.midX, y: settingsBackground.frame.minY+(0.5*credits.frame.height))
        credits.zPosition = 2
        
        self.addChild(background)
        self.addChild(settingsBackground)
        self.addChild(titleLabel)
        self.addChild(backToMenuLabel)
        self.addChild(clearLeaderBoardButton)
        self.addChild(credits)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches{
            let location = touch.location(in: self)
            let touchedNode = self.nodes(at: location)
            let transition1 = SKTransition.flipHorizontal(withDuration: 0.5)
            for node in touchedNode{
                
                if node.name == "backToMenu" {
                    let menuScene = MenuScene()
                    menuScene.scaleMode = .resizeFill
                    self.view?.presentScene(menuScene, transition: transition1)
                    self.removeAllChildren()
                }else if node.name == "clearLeaderBoard" {
                    
                    let userDefaults = UserDefaults.standard
                    let leaderBoardData2: [playerData] = []
                    userDefaults.set(try? PropertyListEncoder().encode(leaderBoardData2), forKey: "leaderBoard")
                    clearLeaderBoardButton.fontSize = 28
                    clearLeaderBoardButton.text = "Leaderboard cleared"
                }else{
                    
                }
                
            }
        }
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
    
}
