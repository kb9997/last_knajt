import Foundation
import SpriteKit
import GameplayKit

class LeaderboardScene: SKScene {

    var background:SKSpriteNode!
    var titleLabel:SKLabelNode!
    var backToMenuLabel:SKLabelNode!
    var leaderboardBackground:SKSpriteNode!
    var leaderboardTextView: UITextView!
    
    override func didMove(to view: SKView) {
        
        self.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        background = SKSpriteNode(imageNamed: "menuBackground")
        background.position = CGPoint(x: 0, y: 0)
        background.zPosition = -1
        background.size =  CGSize(width: self.frame.size.width, height: self.frame.size.height)
        
        titleLabel = SKLabelNode(text: "Leaderboard")
        titleLabel.fontName = "Georgia-Bold"
        titleLabel.fontSize = 30
        titleLabel.fontColor = UIColor.white
        titleLabel.position = CGPoint(x: 0, y: self.frame.height/2-(1.5*titleLabel.fontSize))
        titleLabel.zPosition = 2
        
        backToMenuLabel = SKLabelNode(text: "Back to Menu")
        backToMenuLabel.fontName = "Georgia-Bold"
        backToMenuLabel.fontSize = 30
        backToMenuLabel.fontColor = UIColor.white
        backToMenuLabel.position = CGPoint(x: (self.frame.width/4)-50, y: -self.frame.height/2+backToMenuLabel.fontSize/2)
        backToMenuLabel.name = "backToMenu"
        
        let leaderboardBackgroundWidth = self.frame.width*0.6
        let leaderboardBackgroundHeight = 2*(self.frame.height/3)
        leaderboardBackground = SKSpriteNode(color: UIColor.black, size: CGSize(width: leaderboardBackgroundWidth, height: leaderboardBackgroundHeight))
        leaderboardBackground.zPosition = 1
        leaderboardBackground.position = CGPoint(x: (self.frame.width/4)-50, y: 0)
        
 
        let leaderboardFrame = CGRect(x: self.frame.width*0.4-25, y: self.frame.height/6, width: self.leaderboardBackground.frame.width, height: self.leaderboardBackground.frame.height)
        leaderboardTextView = UITextView(frame: leaderboardFrame)
        leaderboardTextView.font = UIFont(name: "Georgia-Bold", size: 25)
        leaderboardTextView.text = ""
        leaderboardTextView.isUserInteractionEnabled = true
        leaderboardTextView.isEditable = false
        leaderboardTextView.isScrollEnabled = true
        leaderboardTextView.backgroundColor = UIColor.clear
        leaderboardTextView.textColor = UIColor.white
        
        let userDefaults = UserDefaults.standard
        var position:Int = 1
        if let data = userDefaults.value(forKey: "leaderBoard") as? Data {
            let leaderBoardData2 = try? PropertyListDecoder().decode(Array<playerData>.self, from: data)
            for i in leaderBoardData2!{
                leaderboardTextView.text.append("\(position). " + "\(String(describing: i.name!)) : \(String(describing: i.score!))" + "\n" )
                position += 1
            }
        }

        self.view?.addSubview(leaderboardTextView)
        
        self.addChild(background)
        self.addChild(leaderboardBackground)
        self.addChild(titleLabel)
        self.addChild(backToMenuLabel)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches{
            let location = touch.location(in: self)
            let touchedNode = self.nodes(at: location)
            let transition1 = SKTransition.flipHorizontal(withDuration: 0.5)
            for node in touchedNode{
                
                if node.name == "backToMenu" {
                    let menuScene = MenuScene()
                    menuScene.scaleMode = .resizeFill
                    self.view?.presentScene(menuScene, transition: transition1)
                    leaderboardTextView.removeFromSuperview()
                    self.removeAllChildren()
                }else{
                    leaderboardTextView.endEditing(true)

                }
                
            }
        }
    }
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
    
    
}
