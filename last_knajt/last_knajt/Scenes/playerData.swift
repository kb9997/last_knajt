//
//  playerData.swift
//  last_knajt
//
//  Created by kb9997 on 14.04.2019.
//  Copyright © 2019 macKawa. All rights reserved.
//

import Foundation

struct playerData: Codable{
    var name:String!
    var score:Int!
    
    init(name: String = "defaultUserName", score: Int){
        self.name = name
        self.score = score
    }
}
