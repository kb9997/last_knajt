//
//  GameViewController.swift
//  last_knajt
//
//  Created by kb9997 on 15/02/2019.
//  Copyright © 2019 macKawa. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController, GameDelegate {
    
    var hardMode:Bool? = false
    override func viewDidLoad() {
        super.viewDidLoad()
        

        if let view = self.view as! SKView? {

            let menuScene = MenuScene()
            menuScene.scaleMode = .resizeFill
            view.presentScene(menuScene)
            
            view.ignoresSiblingOrder = true
            
            view.showsFPS = false
            view.showsNodeCount = false
        }
        
    }
    func gameOver() {
        performSegue(withIdentifier: "gameOverSegue", sender: nil)
    }
    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
