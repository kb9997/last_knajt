//
//  MenuScene.swift
//  
//
//  Created by kb9997 on 27.02.2019.
//

import Foundation
import SpriteKit
import GameplayKit

class MenuScene: SKScene {
    
    var titleLabel:SKLabelNode!
    var titleLabelShadow:SKLabelNode!
    var background:SKSpriteNode!
    var normalButton:SKSpriteNode!
    var hardButton:SKSpriteNode!
    var settingsButton:SKSpriteNode!
    var leaderboardButton:SKSpriteNode!
    
    override func didMove(to view: SKView) {
        
        let buttonWidth = (self.frame.size.width/3)
        let buttonHeight = (self.frame.size.height/2)*0.5
        
        self.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        background = SKSpriteNode(imageNamed: "menuBackground")
        background.position = CGPoint(x: 0, y: 0)
        background.zPosition = -1
        background.size =  CGSize(width: self.frame.size.width, height: self.frame.size.height)
        
        titleLabel = SKLabelNode(text: "last_knajt")
        titleLabel.fontName = "Georgia-Bold"
        titleLabel.fontSize = 40
        titleLabel.fontColor = UIColor.white
        titleLabel.position = CGPoint(x: -self.frame.size.width/4, y: self.frame.height/2-(1.5*titleLabel.fontSize))
        titleLabel.zPosition = 2
        
        titleLabelShadow = SKLabelNode(text: "last_knajt")
        titleLabelShadow.fontName = "Georgia-Bold"
        titleLabelShadow.fontSize = 40
        titleLabelShadow.fontColor = UIColor.black
        titleLabelShadow.position = CGPoint(x: -self.frame.size.width/4+5, y: self.frame.height/2-(1.5*titleLabel.fontSize)+5)
        titleLabelShadow.zPosition = 1
        
        normalButton = SKSpriteNode(imageNamed: "menuNormalclaws")
        normalButton.scale(to: CGSize(width: buttonWidth, height: buttonHeight))
        normalButton.position = CGPoint(x: self.frame.size.width/4, y: 100)
        normalButton.name = "normalButton"
        
        hardButton = SKSpriteNode(imageNamed: "menuHard")
        hardButton.scale(to: CGSize(width: buttonWidth, height: buttonHeight))
        hardButton.position = CGPoint(x: self.frame.size.width/4, y: 0)
        hardButton.name = "hardButton"
        
        settingsButton = SKSpriteNode(imageNamed: "settings")
        settingsButton.scale(to: CGSize(width: buttonWidth/2, height: buttonHeight))
        settingsButton.position = CGPoint(x: self.frame.size.width/4-settingsButton.frame.width/2, y: -100)
        settingsButton.name = "settingsButton"
        
        leaderboardButton = SKSpriteNode(imageNamed: "trophy")
        leaderboardButton.scale(to: CGSize(width: buttonWidth/2, height: buttonHeight))
        leaderboardButton.position = CGPoint(x: self.frame.size.width/4+leaderboardButton.frame.width/2, y: -100)
        leaderboardButton.name = "leaderboardButton"
        
        self.addChild(background)
        self.addChild(titleLabel)
        self.addChild(titleLabelShadow)
        self.addChild(normalButton)
        self.addChild(hardButton)
        self.addChild(settingsButton)
        self.addChild(leaderboardButton)  
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches{
            let location = touch.location(in: self)
            let touchedNode = self.nodes(at: location)
            let transition1 = SKTransition.flipHorizontal(withDuration: 0.5)
            for node in touchedNode{
            
                if node.name == "normalButton" {
                    
                    let gameScene1 = GameScene(fileNamed: "GameScene")
                    gameScene1?.scaleMode = .resizeFill
                    self.view?.presentScene(gameScene1!, transition: transition1)
                    self.removeAllActions()
                    self.removeAllChildren()
                    
                }else if (node.name == "hardButton"){
                    
                    let gameScene2 = GameScene(fileNamed: "GameScene")
                    gameScene2?.hardMode = true;
                    gameScene2?.scaleMode = .resizeFill
                    self.view?.presentScene(gameScene2!, transition: transition1)
                    self.removeAllActions()
                    self.removeAllChildren()
                    
                }else if node.name == "leaderboardButton" {
                    
                    let leaderboardScene1 = LeaderboardScene()
                    leaderboardScene1.scaleMode = .resizeFill
                    self.view?.presentScene(leaderboardScene1, transition: transition1)
                    self.removeAllActions()
                    self.removeAllChildren()
                    
                }else if node.name == "settingsButton" {
                    
                    let settingsScene1 = SettingsScene()
                    settingsScene1.scaleMode = .resizeFill
                    self.view?.presentScene(settingsScene1, transition: transition1)
                    self.removeAllActions()
                    self.removeAllChildren()
                    
                }else{
                    
                    }
                
            }
        }
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
    
}
