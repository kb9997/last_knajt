//
//  sceneContainer.swift
//  
//
//  Created by macKawa on 04.03.2019.
//

import Foundation
class sceneContainer {
    var menuScene1:MenuScene? = nil
    var gameScene1:GameScene? = nil
    var overScene1:OverScene? = nil
    
    func initScenes(){
        menuScene1 = MenuScene()
        gameScene1 = GameScene()
        overScene1 = OverScene()
    }
}
